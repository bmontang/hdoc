Optim to Opale (`optim_to_opale`)
=================================
Last edit: January 7, 2016

License
-------
This project is under [GPL 3.0 license](http://www.gnu.org/licenses/gpl-3.0.txt).

Credits
-------
### Autumn 2015

* Ivan D'HALLUIN
* Jeffrey DECORDE
* Jean-Baptiste MARTIN

Presentation
------------
`optim_to_opale` is an ANT script that converts files from Optim format to Opale format. It simply calls `optim_to_hdoc` and `hdoc_to_opale` scripts.

Dependencies
------------
In order to work properly this script needs

1. [`optim_to_hdoc`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/optim_to_hdoc) (Optim to HDoc conversion)
2. [`hdoc_to_opale`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_opale) (HDoc to Opale conversion)

User Documentation
------------------
### Running the script

1. Put the file (`.scar`) you want to convert into the `input` directory (you can convert paper, section, website, slideshow and webpage)
2. Run the `run.sh` file on a Unix system (Linux, Mac OS, etc.) or the `run.bat` on Windows
3. Your converted file is placed into the `output` directory (a log file is also placed into the `log` directory)
4. Enjoy!

Unsupported
-----------

### Single file conversion with parameter

The script currently doesn't support any paramater to specify the file to convert. If multiple files are in the `input` directory, the script will convert all these files.

Known bugs
----------
N/A for now.

Todo
----

### Single file conversion with parameter

This feature requires that the same feature has been implementing in `optim_to_hdoc` and `hdoc_to_opale` first.

1. Add a parameter to the `ant` script and to both the `.bat` and `.sh` scripts to accept a filename (refering to a file placed into the `input` directory) to convert only one file
2. Don't clean `optim_to_hdoc` and `hdoc_to_opale` `input` directories: simply remove the lines in the `clean` target
3. Call `optim_to_hdoc` and `hdoc_to_opale` scripts by passing a parameter with the filename (this feature first needs to be implementing in `optim_to_hdoc` and `hdoc_to_opale`

Technical notes
---------------

### How is this script working

1. The script first determines what OS you are using so it then can call the right scripts
2. It cleans the `log`, `tmp` and `output` directories
3. It copies the files that are inside the `input` directory to the `input` directory of `optim_to_hdoc`
4. It calls `optim_to_hdoc` run script (`.bat` on windows, `.sh` on Unix)
5. It copies the files that are inside the `output` directory of `optim_to_hdoc` to the `input` directory of `hdoc_to_opale`
6. It calls `hdoc_to_opale` run script (`.bat` on windows, `.sh` on Unix)
7. It copies the files that are inside the `output` directory of `hdoc_to_opale` to the `output` directory of `optim_to_opale`

During all the process it also prints some basic information both on screen and in the log file.

Capitalization
--------------
N/A for now
