# Hdoc Converter Projet

License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

Credits : 
Université de Technologie de Compiègne (http://www.utc.fr)
NF29 students (http://www4.utc.fr/~nf29)

## What is Hdoc ?

Please refer to the Hdoc Converter Project website: 

http://hdoc.crzt.fr

## What is this repository ?
This repository gathers some of the Hdoc converters, if not all of them.

Project URL : https://gitlab.utc.fr/crozatst/hdoc.git

## How to use Hdoc Converters ?

In order to use a converter, choose the corresponding folder and consult README.md for instructions.
