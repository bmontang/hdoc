<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:h="http://www.utc.fr/ics/hdoc/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  >
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
  <xsl:param name="filename"/>

  <xsl:template match="sc:item">
    <project name="copy-ucs" basedir="../.." default="main">
      <property name="in" location="${{basedir}}/input"/>
      <property name="out" location="${{basedir}}/output"/>
      <property name="tmp" location="${{basedir}}/tmp"/>
      <property name="xsl" location="${{basedir}}/xsl"/>
      <property name="lib" location="${{basedir}}/lib"/>
      <property name="log" location="${{basedir}}/log"/>
      <property name="schema" location="${{basedir}}/schema"/>
      <property name="filename" location="${$filename}"/>
      <taskdef name="jing" classname="com.thaiopensource.relaxng.util.JingTask">
        <classpath>
          <pathelement location="../${lib}/jing.jar"/>
        </classpath>
      </taskdef>

      <property file="global.properties"/>

      <target name="main">
        <xsl:apply-templates select=".//sp:courseUc"/>
      </target>
    </project>
  </xsl:template>

  <xsl:template match="sp:courseUc">
    <xslt
        in="${{tmp}}/${{filename}}/outputWithCourseUcIds.xml"
        out="${{tmp}}/${{filename}}/decompressedOpaleDivided/{@data-export-file}"
        style="${{xsl}}/copyCourseUc.xsl"
        processor="org.apache.tools.ant.taskdefs.optional.TraXLiaison"
    >
      <param name="elementid" expression="{@data-export-id}"/>
    </xslt>

    <trycatch property="foo" reference="bar">
      <try>
        <jing file="${{tmp}}/${{filename}}/decompressedOpaleDivided/{@data-export-file}" rngfile="${{schema}}/op_expUc.rng"></jing>
      </try>
      <catch>
        <echo>Validation failed</echo>
      </catch>
    </trycatch>

  </xsl:template>
</xsl:stylesheet>
