<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<project name="hdoc_to_opale" basedir="." default="convert">

    <taskdef resource="net/sf/antcontrib/antlib.xml"/>
    <taskdef name="jing" classname="com.thaiopensource.relaxng.util.JingTask"/>

    <property name="in" location="${basedir}/input"/>
    <property name="out" location="${basedir}/output"/>
    <property name="tmp" location="${basedir}/tmp"/>
    <property name="xsl" location="${basedir}/xsl"/>
    <property name="lib" location="${basedir}/lib"/>
    <property name="log" location="${basedir}/log"/>
    <property name="schema" location="${basedir}/schema"/>
    <property name="bibtex" location="${basedir}/bibtex_to_opale"/>
    <property file="global.properties"/>

  	<target name="convert">

      <delete dir="${tmp}" failonerror="false"/>
      <sleep seconds="1"/>
      <mkdir dir="${tmp}"/>
      <delete dir="${out}" failonerror="false"/>
      <sleep seconds="1"/>
      <mkdir dir="${out}"/>
  	  <delete dir="${log}" failonerror="false"/>
      <sleep seconds="1"/>
      <mkdir dir="${log}"/>

      <for param="inputFile">
        <path>
          <fileset dir="${in}" includes="**/*.hdoc"/>
        </path>
        <sequential>
          <local name="filename"/>
          <basename property="filename" file="@{inputFile}"/>

      		<antcall target="UnzipHdocFile">
            <param name="filename" value="${filename}"/>
          </antcall>

      		<antcall target="ValidateInput">
            <param name="filename" value="${filename}"/>
          </antcall>

      		<antcall target="FindContentFiles">
            <param name="filename" value="${filename}"/>
          </antcall>

    	    <ant antfile="${tmp}/${filename}/generateContentPath.xml">
            <property name="filename" value="${filename}"/>
          </ant>

      		<antcall target="ValidateOutput">
            <param name="filename" value="${filename}"/>
          </antcall>

          <antcall target="DivideOutput">
            <param name="filename" value="${filename}"/>
          </antcall>

		      <antcall target="ZipOutput">
            <param name="filename" value="${filename}"/>
          </antcall>

          <antcall target="ZipDividedOutput">
            <param name="filename" value="${filename}"/>
          </antcall>

        </sequential>
      </for>
    </target>

  	<target name="UnzipHdocFile">
  		<!-- Unzip the input hdoc file. Decompressed folder is named "decompressedHdoc" : this name is the only one which
  		refers to the hdoc file furthermore in this project. -->
      <unzip src="${in}/${filename}" dest="${tmp}/${filename}/decompressedHdoc"/>
      <chmod dir="${tmp}/${filename}/decompressedHdoc" perm="777"/>
  	</target>

	<target name="FindContentFiles">
		<!-- Finds the absolute path of container.xml and applies transformation0.xsl on it.-->
    <first id="first">
		    <fileset dir="${tmp}/${filename}/decompressedHdoc/META-INF" includes="**/container.xml"/>
		</first>
		<xslt in="${toString:first}" out="${tmp}/${filename}/generateContentPath.xml" style="${xsl}/transformation0.xsl" processor="org.apache.tools.ant.taskdefs.optional.TraXLiaison">
      <param name="filename" expression="${filename}"/>
      <param name="lib" expression="${lib}"/>
    </xslt>
		<chmod file="${tmp}/${filename}/generateContentPath.xml" perm="777"/>
	</target>

	<target name="ZipOutput">
    <propertyregex property="properFilename" input="${filename}" regexp=".hdoc" replace="" casesensitive="false" override="true" />
		<copy file="${bibtex}/.wspmeta" todir="${tmp}/${filename}/decompressedOpale"/>
		<mkdir dir="${tmp}/${filename}/decompressedOpale/res"/>
		<ant antfile="${tmp}/${filename}/moveRessourceFiles.xml"/>
		<zip basedir="${tmp}/${filename}/decompressedOpale" destfile="${out}/${properFilename}/output.scar" encoding="UTF-8"/>
  </target>

	<target name="ZipDividedOutput">
    <propertyregex property="properFilename" input="${filename}" regexp=".hdoc" replace="" casesensitive="false" override="true" />
    <copy file="${bibtex}/.wspmeta" todir="${tmp}/${filename}/decompressedOpaleDivided"/>
		<copy todir="${tmp}/${filename}/decompressedOpaleDivided/res" >
	        <fileset dir="${tmp}/${filename}/decompressedOpale/res" includes="**"/>
	 	</copy>
		<copy todir="${tmp}/${filename}/decompressedOpaleDivided/references" >
	        <fileset dir="${tmp}/${filename}/decompressedOpale/references" includes="**"/>
	 	</copy>
		<zip basedir="${tmp}/${filename}/decompressedOpaleDivided" destfile="${out}/${properFilename}/dividedOutput.scar" encoding="UTF-8"/>
  </target>

	<!-- Validating the XML container file -->
	<target name="ValidateInput">
    <trycatch property="foo" reference="bar">
      <try>
        <jing file="${tmp}/${filename}/decompressedHdoc/META-INF/container.xml" rngfile="${schema}/hdoc1-container.rng"></jing>
      </try>
      <catch>
        <echo>Validation failed</echo>
      </catch>
    </trycatch>
	</target>

	<!-- Validating the XML output -->
	<target name="ValidateOutput">
    <trycatch property="foo" reference="bar">
      <try>
        <jing file="${tmp}/${filename}/decompressedOpale/main.xml" rngfile="${schema}/op_ue.rng"></jing>
      </try>
      <catch>
        <echo>Validation failed</echo>
      </catch>
    </trycatch>
	</target>

	<target name="DivideOutput">
    <mkdir dir="${tmp}/${filename}/decompressedOpaleDivided"/>

		<!-- Adding IDS to the general output file -->
		<xslt
      in="${tmp}/${filename}/decompressedOpale/main.xml"
      out="${tmp}/${filename}/outputWithCourseUcIds.xml"
      style="${xsl}/addCourseUcIds.xsl"
      processor="org.apache.tools.ant.taskdefs.optional.TraXLiaison"
		/>

		<!-- Generating the root file (with refs to other files) -->
		<xslt
      in="${tmp}/${filename}/outputWithCourseUcIds.xml"
      out="${tmp}/${filename}/decompressedOpaleDivided/main.xml"
      style="${xsl}/addCourseUcReferences.xsl"
      processor="org.apache.tools.ant.taskdefs.optional.TraXLiaison"
		/>

		<!-- Generating the ANT file that will copy the files -->
		<xslt
      in="${tmp}/${filename}/outputWithCourseUcIds.xml"
      out="${tmp}/${filename}/exportUnits.ant"
      style="${xsl}/prepareCourseUcCopies.xsl"
      processor="org.apache.tools.ant.taskdefs.optional.TraXLiaison"
		>
      <param name="filename" expression="${filename}"/>
    </xslt>

		<!-- Executing that ANT file -->
		<ant antfile="${tmp}/${filename}/exportUnits.ant"/>
	</target>

</project>
