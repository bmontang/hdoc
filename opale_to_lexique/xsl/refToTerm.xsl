<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:lx="scpf.org:lexicon"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:op="utc.fr:ics/opale3"
    
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="xml" indent="yes" />
    
    <xsl:template match="/">
        <sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
            <lx:term xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:lx="scpf.org:lexicon">
                <lx:termM>
                    <sp:name><xsl:value-of select="sc:item/op:glos/op:glosM/sp:term"></xsl:value-of></sp:name>                    
                </lx:termM>
                <sp:def>
                    <lx:definition>
                        <sp:desc>
                            <lx:defTxt>
                                <xsl:apply-templates select="sc:item/op:glos/op:glosM/sp:def/op:sTxt/*"></xsl:apply-templates>
                            </lx:defTxt>
                        </sp:desc>
                    </lx:definition>
                </sp:def>
            </lx:term>
        </sc:item>
    </xsl:template>
    
    <xsl:template match="op:sTxt//*|op:sTxt//@*" >
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- filtrage du contenu -->
    
    <xsl:template match="op:urlM" priority="2">
        <lx:urlM>
            <xsl:apply-templates select="*"/> 
        </lx:urlM>
    </xsl:template>
    
    <!-- transforme les termes étrangers de Opale en termes spécifiques de Lexique -->
    <xsl:template match="sc:inlineStyle[@role='spec']" priority="2">
        <sc:phrase role="special"><xsl:apply-templates select="*|text()"></xsl:apply-templates></sc:phrase>
    </xsl:template>
    
    <!-- remplace 'emp' par 'emphasis' -->
    <xsl:template match="sc:inlineStyle/@role[.='emp']" priority="2">
        <xsl:attribute name="role">emphasis</xsl:attribute>
    </xsl:template>
    
</xsl:stylesheet>