# Opale to Mongo (`opale_to_mongo`)

## License
License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

## Credits
Alexandre Thouvenin
Kapilraj Thangeswaran

## Presentation
This module is able to extract data from a file in Opale format and insert them into MongoDB.

## Dependencies
In order to work properly this module needs

1. [`opale_to_hdoc`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/opale_to_hdoc) (Opale to Hdoc conversion)
2. [`hdoc_to_mongo`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_mongo) (Hdoc to Mongo conversion)