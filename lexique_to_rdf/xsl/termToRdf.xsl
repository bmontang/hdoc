<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:ont="nf29ont"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:lx="scpf.org:lexicon"
    exclude-result-prefixes="xs sp sc lx"
    version="2.0">
    
    <xsl:output method="xml" indent="yes" />
    
    <xsl:template  match="/">            
                    <rdf:Description>
                        <xsl:attribute name="about">
                            <xsl:value-of select="sc:item/lx:term/lx:termM/sp:name"></xsl:value-of>
                        </xsl:attribute>
                        <ont:def>
                            <xsl:value-of select="sc:item/lx:term/sp:def/lx:definition/sp:desc/lx:defTxt/sc:para"></xsl:value-of>
                        </ont:def>
                        <ont:source>
                            <xsl:value-of select="sc:item/lx:term/sp:def/lx:definition/sp:source/@sc:refUri"/>
                        </ont:source>
                        <ont:tag>
                            <xsl:value-of select="sc:item/lx:term/sp:index/lx:index/sp:tag/@sc:refUri"></xsl:value-of>
                        </ont:tag>
                    </rdf:Description>
    </xsl:template>
    
   
</xsl:stylesheet>
