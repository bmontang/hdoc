# Hdoc to Mongo (`hdoc_to_mongo`)

## License
License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

## Credits
Alexandre Thouvenin
Kapilraj Thangeswaran

## Presentation
This module is able to extract data from a file in Hdoc format and insert them into MongoDB.

## Dependencies
No dependance needed.