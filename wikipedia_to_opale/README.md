Converter : Wikipedia To Opale
==============================

Licence : 
---------------
GPL 3.0
http://www.gnu.org/licenses/gpl-3.0.txt


Credits :
---------------
Carrel Billiard Harold

Harriga Merouane

Lhomme Nicolas

Previous developers


Presentation
------------

Wikipedia to Opale is a converter that transforms Wikipedia pages to Opale.

Dependence
---------
-   Wikipedia To Hdoc Converter
-   Hdoc to Opale Converter


User Documentation
------------------

Generating .hdoc of a Wikipedia article with an URL
---------------------------------------------------

1 - Run the command corresponding to your OS
        
        On windows : 
            runURL.bat yourWikipediaUrl yourFilename
                yourWikipediaUrl is the Wikipedia URL
                yourFilename is the name of the directory in which output files will be placed
                
            For instance : runURL.bat https://fr.wikipedia.org/wiki/Constructeur_(programmation) constructeur
        
        On Linux : 
            sh runURL.sh yourWikipediaUrl yourFilename
                yourWikipediaUrl is the Wikipedia URL
                yourFilename is the name of the directory in which output files will be placed
            
            For instance : sh runURL.sh https://fr.wikipedia.org/wiki/Constructeur_(programmation) constructeur
            
2 - Get the .scar in the output/yourFilename folder

Generating .hdoc of a Wikipedia article with a local file
---------------------------------------------------------

1 - Copy the content of the Wikipedia article you want to convert in the directory named “input” and in a file called “source.xml".
    Display the source code of the wikipedia page, copy it and paste it in the new file source.xml
    Make sure to copy/paste the source code and not save it directly as a file.
    
2 - Run the command corresponding to your OS
        
        On windows : 
            runFile.bat
        
        On Linux : 
            sh runFile.sh
                       
3 - Get the .scar in the output/source folder

BUG
---

1 - Linux sh files doesn't work with UTC proxy but works outside UTC

Unsupported
-----------
1   Images:
    -   Images inside text are not supported because of schema validation.

To do
-----
1   Images:
    -   Do a preposition to modify the hdoc schema so that we will be able to manage images inside text
    -   Complete the extraction of the metadata information of images



Technical notes
---------------
For images you can refer to the get-ressources-with-meta.xsl and official-meta.xml in the hdoc_to_wikipedia/xslt Folder Read the commentary. It will help you to finish the task regarding images. These files are included to give you a solution to start from.
