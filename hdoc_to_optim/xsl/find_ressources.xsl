﻿<!--find_ressources.xsl creates a ANT file get_ressources-->
<!--the ANT copies all audio files, images and objects used in the content to convert-->
<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:h="http://www.utc.fr/ics/hdoc/xhtml"
	xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output method="xml" indent="yes"/>
	
    <xsl:template match="h:html">
    	<project name="hdoc_to_optim" default="main">
			<target name="main">
				<xsl:apply-templates select="h:body"/>
			</target>
		</project>	
	</xsl:template>
	
	<xsl:template match="h:body">
		<xsl:apply-templates select="h:section"/>
	</xsl:template>
	
	<xsl:template match="h:section">
		<xsl:apply-templates select="h:div"/>
		<xsl:apply-templates select="h:section"/>
	</xsl:template>
		
	<xsl:template match="h:div">
		<xsl:apply-templates select="h:section"/>
		<xsl:apply-templates select="h:img"/>
		<xsl:apply-templates select="h:object"/>
		<xsl:apply-templates select="h:audio"/>
		<xsl:apply-templates select="h:video"/>
	</xsl:template>
	
	<xsl:template match="h:img">	
		<xsl:variable name="src" select="@src"/>
		<copy file="hdoc/{$src}" tofile="result/{$src}"/>
	</xsl:template>
	
	<xsl:template match="h:object">	
		<xsl:variable name="data" select="@data"/>
		<copy file="hdoc/{$data}" tofile="result/{$data}"/>
	</xsl:template>
	
	<xsl:template match="h:audio">	
		<xsl:variable name="src" select="@src"/>
		<copy file="hdoc/{$src}" tofile="result/{$src}"/>
	</xsl:template>
	
	<xsl:template match="h:video">	
		<xsl:variable name="src" select="@src"/>
		<copy file="hdoc/{$src}" tofile="result/{$src}"/>
	</xsl:template>
</xsl:stylesheet>



		
				
	
		

	