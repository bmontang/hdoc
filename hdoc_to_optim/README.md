﻿Converter hdoc_to_optim
-----------------------

The purpose of this converter is to obtain an Optim document (paper, slideshow, website or webpage) from a HTML file that respects the HDOC schema.


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Rihab HACHEM

* Perrine RAVAUD (2014).


Dependance
----------

No dependance needed.


User documentation
------------------

There are two different ways to use the converter hdoc_to_optim: by running a script run.bat/run.sh or by command line using a terminal (allows the user to specify some parameters like the document type).


#### Running the script run.bat/run.sh:

Use this method if you do not want to use a terminal. 

1. Download hdoc_converter.zip and unzip it.
2. Add your source file to the Input folder. It has to be a .hdoc file. 
3. Place only one file in that folder !!! 
4. If you are using Linux, run the script run.sh. If you are using Windows, run the script run.bat. 
5. Your file has been converted, the result is in the Output folder. 
6. You can now open it with OptimOffice.

The default output document type used by this script is an Optim paper. 


#### Terminal: 

By using the terminal you can specify some parameters to the conversion : the source file, or the output type (either a paper, a slideshow, a website or a webpage).

1. Download hdoc_converter.zip and unzip it.
2. Open your terminal and go into the folder hdoc_to_optim.
3. Run the following command:

        "ant -buildfile hdoc_to_optim.ant"
    
    You can specify the source file, and the output document type by adding parameters.
    Use -DInputFile to specify the source file. 
    Use -DDocType to specify if it's a "paper", a "slideshow", a "website" or a "webpage".
    Exemple:
        
        "ant -buildfile hdoc_to_optim.ant -DDocType slideshow -DInputFile sample.hdoc"

	"ant -buildfile hdoc_to_optim.ant -DDocType website"


Both parameters are optional. Your file has been converted, you can open your paper with OptimOffice.


Unsupported 
-----------

#### In paper, slideshow, website and webpage: 

* Inline: Superscript, Subscript
* SpanType : Latex, Syntax

#### In slideshow, website and webpage:

* Metadata : Description, Keywords

#### In webpage:

* All metadata (however, if it's only a webpage, is it really useful ? The metadata should appear on the website information, not each webpage ? )


Known bugs
----------

No known bugs now.


Todo
---- 

* Offer the possibility of integrating the sections into a webmedia application.
* Find a better way to transform the metadata (description and keywords) information.
* Factorise the code (put all transformations in one file for example).
* Offer the possibility of placing multiple input files.
* Integrate Concept trees.


Technical notes
---------------

The converter contains 5 files:

* hdoc_to_optim.ant : 
When you launch the Converter (whether through the script or a terminal) it's the file hdoc_to_optim.ant that is launched, and executes the following files.

* find_content.xsl : 
This XSLT program runs through the hdoc file container.xml to retrieve the path of the file to convert and creates a ANT script get_content.ant.

* find_ressources.xsl :
This XSLT program runs through the content file of hdoc  to retrieve the path of each audio files, images and objects used in it and creates a ANT script get_ressources.ant.

* transfo.xsl (paper) :
This XSLT program runs through the content file of hdoc and convert each hdoc tag in Optim tags if possible. It converts it to a paper.

* transfo2.xsl (slideshow) :
This XSLT program runs through the content file of hdoc and convert each hdoc tag in Optim tags if possible. It converts it to a slideshow.

* transfo3.xsl (website) :
This XSLT program runs through the content file of hdoc and convert each hdoc tag in Optim tags if possible. It converts it to a website.

* transfo4.xsl (webpage) :
This XSLT program runs through the content file of hdoc and convert each hdoc tag in Optim tags if possible. It converts it to a webpage.

* .wspmeta :
This file contains Optim metadata informations, it must be placed in the result of the conversion


Capitalisation
--------------

N/A
