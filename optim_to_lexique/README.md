# Converter optim_to_lexique
Converter optim_to_lexique

License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

Crédits
Florian Arend
Estelle de Magondeaux
Marouane Hammi
Antoine Aufrechter

Install
In order to use this converter, follow those steps :
1. Copy your Optim file(s) (.scar or .zip) into the input directory.
2. Execute the run that correspond to your OS.
3. You will find the result into the output directory. The name of the output files depends on the hour it was processed.

Dependance

User documentation

Unsupported

Known bugs

TODO

Capitalisation

