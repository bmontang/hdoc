Converter Mindmapping2HDOC
==========================

Conversion of Freemind file(s) (.mm) to Hdoc.

Licence
-------
http://www.gnu.org/licenses/gpl-3.0.txt

Credits
-------

* Amélie Perdriaud
* Thibault Brocheton
* Bastien Frémondière
* Guillaume Gomez

Presentation
------------
This project aims at converting mindmapping files (.mm) to hdoc files. If you use another tool to create mindmapping files, you can probably export it to a .mm file and use it in this converter.

User documentation
------------------

* Use a terminal and go to the root of the folder (mindmapping_to_hdoc).
* Please refer to the part *Rules to follow* to understand the rules that can help you to personalize the output
* Put your files in a folder 'mindmapping_to_hdoc/input'. The file has to be a Freemind file (.mm).
* Enter the command line corresponding to your OS :
	* On Linux : 'sh run.sh'
	* On Windows : 'run.bat'
If you want to convert one specific file that is in the input folder, use the parameters ' -DinputPath input/<yourFilename>' (ex : 'sh run.sh -DinputPath input/<yourFilename>')

*You will find the result of the conversion in the folder mindmapping_to_hdoc/output*
*You can find a sample file il the folder /samples.*

You can use the converter mindmapping_to_opale in order to create Opale contents.

Unsupported
-----------

* Convert color
* Convert icon into summary elements

Known bugs
----------

Todo
----

* Mindmap to Lexique conversion
* Mindmap to Optim conversion
* Check the compatibility of version and software (files exported from Xmind etc.)

Technical notes
---------------
This converter is using standard NF29 conversion project structure : I use a main ANT file (named mapMapping2Hdoc.ant), which handles routine tasks (zipping archives, copying files, order tasks), XSL-XSLT transformation scripts calls. This main ANT file is composed of several targets.
During the conversion process, I use a few temporary files stored in a temp folder : their content depends on hdoc's files (such as "container.xml", "content.xml" and ressource files). See details below.
You can comment the part of code where temp directory is deleted in order to view raw files directly instead of unzipping hdoc file.

What is META-INF folder ?
This is a simple folder with a xml file named 'content.xml' that contains useful information for Opale. Every .scar archives must contain this file, what it exactly does is not the purpose of this project.


Capitalisation
--------------
