Doc du pseudo langage:
======================

Idée générale:
- Le parser réalise une arborescence parfaite, en mettant le node racine en Module, les nodes suivants en unités et les feuilles en Grains.
- Un pseudo langage est définit pour modifier ce comportement de base.

Ordonnancement:
---------------

*base:* Récupération dans l'odre du fichier .mm
*langage:* #1 , #2, ... #n  OU order(n) pour gérer l'ordonnancement entre les nodes d'un même père. 

Gestion des balisages:
----------------------

*base:* application du comportement de base si pas de balisage
*langage:*
- #p OU p() : <p></p>
- #intro OU introduction : voir 
- #conclu OU conclusion : 
