<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns:regexp="http://exslt.org/regular-expressions" extension-element-prefixes="regexp">
    
    
    <xsl:output method="xml" indent="yes" />
    <xsl:strip-space elements="*"/>
    
    <xsl:param name="text-encoding" as="xs:string" select="'utf-8'"/>
    <xsl:param name="text-uri" as="xs:string" select="'../tmp/pad-clean.xml'"/>
    
    <xsl:template name="text2xml">
        <xsl:variable name="text" select="unparsed-text($text-uri, $text-encoding)"/>
        <xsl:analyze-string select="$text" flags="s" regex=".*title.(.*?)./title>.*.style.*/style.*body.(.*)./body">    
            <xsl:matching-substring>
                <head>
                    <title>
                        <xsl:value-of select="normalize-space(regex-group(1))"/>
                    </title>
                    <meta charset="utf-8"/>
                </head>
                <body>
                    <div>
                        <p>
                            <xsl:value-of select="replace(normalize-space(regex-group(2)), '&lt;br&gt;', '&lt;br/&gt;')" disable-output-escaping="yes"/>
                        </p>
                    </div>
                </body>
            </xsl:matching-substring>
        </xsl:analyze-string>
        
    </xsl:template>
    
    
    <xsl:template match="/">
        <html lang="en">
            <xsl:choose>
                <xsl:when test="unparsed-text-available($text-uri, $text-encoding)">
                    <xsl:call-template name="text2xml"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="error">
                        <xsl:text>Error reading "</xsl:text>
                        <xsl:value-of select="$text-uri"/>
                        <xsl:text>" (encoding "</xsl:text>
                        <xsl:value-of select="$text-encoding"/>
                        <xsl:text>").</xsl:text>
                    </xsl:variable>
                    <xsl:message>
                        <xsl:value-of select="$error"/>
                    </xsl:message>
                    <xsl:value-of select="$error"/>
                </xsl:otherwise>
            </xsl:choose>
        </html>
    </xsl:template>
    
</xsl:stylesheet>
