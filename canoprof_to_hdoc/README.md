# Convertisseur Canoprof vers HDOC

## User story
-L'utilisateur ouvre Canoprof
-Il cr�e son contenu
-Il fait un clic droit sur l'�lement principal (la racine du contenu) et clique sur exporter l'archive
-Il ne touche pas aux options
-Il sauvegarde le .SCAR dans le dossier input du convertisseur
-Il lance le fichier run.bat (run.sh sous linux)
-Il r�cup�re le hdoc cr�� dans le dossier output
